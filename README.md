# Hi there, I'm Jon

Whether we're just meeting, or getting to know each other better, I'm looking forward to working together. This document outlines some things about me including my values, priorities, and way of working. I hope this helps set the expectations of how we'll work together.

## Lightly Seared On The Reality Grill  (the backstory)

I have a lot of experience in building and running software of all different types.  I've worked as a developer, I've run teams, I've built up organisations from small to large, and I've also been in technical management, a role that I didn't enjoy but have the greatest respect for those who have done it.

Most of my experience comes from building very large software systems, specifically the software.  I have enough experience in networking and operating system administration to know my limitations and defer to those with greater  skills in those areas.

Having a lot of experience doesn't make me smarter, and I certainly never feel that way - but the advantage of experience in the fashion oriented world of technology is "everything old is new again" and the cycles repeat, so I've probably seen this "thing" in another name that you're trying to do right now.

## Expectation (the contract)

What do I expect from you

1. To be honest and open in our interactions, even if this is scary and hard
2. To consider the needs of the company, the team and you as an individual in decision making
3. To ask me questions to help you make better choices
4. To ask for clarification on topics and interactions between us that are confusing
5. To understand that when I share my experience on a topic it is not a directive and you should consider the applicability to your situation


What can you expect from me

1. To be honest and open in our interactions
2. To be trustworthy with any information you share with me
3. To support you, and/or act as your proxy should you wish to provide information to others when you do not feel comfortable in doing so directly
4. To respect your opinions, even if I may not agree with them
5. To support your choices and help you to succeed with them to the best of my ability, even if I may not agree with them
6. To allow you to learn and fail safely
7. To help you grow your career through the development of your skills.  Where I do not have the skills personally, I promise I will connect you with people with those skills to enable your growth
8. To have a wide, but not necessarily deep view across all the activities in our organisation
9. To keep up to date with technical activities that the teams are performing
10. To keep writing code and using a variety of technologies, even if only for personal use to remain aware of what is happening in the industry

## Values (the things I like and lean towards)

1. Clear processes, practices, roles, accountability and responsibility
2. Making feedback occur as often as possible and listening to it
3. Giving you the right levels of accountability and responsibility, and holding you to them
4. Understanding the problem must happen before you can develop a solution
5. Teaching, not lecturing
6. Mentoring, not managing
7. Autonomy on products of work, not practices of work
8. Guard rails and standards, not governance
9. Trusting, but verifying
10. Planning matters, plans don't
11. Estimating matters, but estimates are not promises
12. Outputs matter, inputs don't
13. Understanding fundamentals matter, implementations don't
14. Naming is powerful
15. Making my customers smile
16. Laughing

## Confusion (frequently encountered issues)

Jon is opinionated.  Yes, yes I am.  I try very hard to give you the information you (and others) need to be successful. I don't really know how to do this without sharing my experiences and stories.  Sometimes this comes across as being directive - that's not the intent.  The intent is to try and save you from some of the many mistakes I made in my career.

You say you value autonomy, but you like standards - how does that work?  Well, glad you asked.  There are 2 parts to work, the "system of work" - which is the part of the work that provides frameworks, processes, practices.  This is how we organise ourselves and how we might structure and sequence activities. The second part is the "product of the work" - this is why we get paid, and why we build things.  This is the part of our work that requires creative intelligence, the system of work is to support us by taking the mundane day-to-day decisions away and freeing up the brain to concentrate on the product. I prefer standards in the system of work, and creativity in the product of work.

But, but, doesn't that mean we can't change the standards?  No, you encode that into the system of work, so that rather than having people doing what they feel like - there's a way to contribute back and make the system of work better for everybody.

